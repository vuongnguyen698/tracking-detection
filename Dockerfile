FROM python:3.8.13-slim-buster

RUN apt-get -u update && apt-get install libgomp1 ffmpeg libsm6 libxext6  -y
RUN mkdir /home/streamlit_detection

WORKDIR /home/streamlit_detection
#RUN python3.8 -m pip install -r python_requirements.txt
RUN pip install ultralytics streamlit pafy youtube-dl
COPY . /home/streamlit_detection
EXPOSE 8501
CMD [ "streamlit", "run", "./app.py" ]
